<?php

use Illuminate\Database\Seeder;
use  App\User;

class UsersTableSeeder extends Seeder
{

    public function run()
    {
        User::create([
            'name' => 'Jhonny',
            'email' => 'jhonny@example.com',
            'password' => bcrypt('123123')
        ]);

        User::create([
            'name' => 'Pedro',
            'email' => 'pedro@example.com',
            'password' => bcrypt('123123')
        ]);

        User::create([
            'name' => 'Ramon',
            'email' => 'ramon@example.com',
            'password' => bcrypt('123123')
        ]);
    }
}
