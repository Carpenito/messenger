<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Message;
use App\Observers\MessageObserver;

class AppServiceProvider extends ServiceProvider
{

    public function boot()
    {
        Message::observe(MessageObserver::class);
    }


    public function register()
    {
        //
    }
}
